import flixel.FlxSprite;
import flixel.util.FlxPath;

class Enemy extends FlxSprite {
    
    /*  Haxe seems to want me to use optional params instead of 
     *  overloading. Any params with a ? are optional.
     */
    public override function new() {
        super();
    }

    public override function update(elapsed:Float) {
        super.update(elapsed);
    }
}