package;

import flixel.FlxState;

class PlayState extends FlxState
{
	var _nonmoving:Enemy;
	override public function create():Void
	{
		super.create();
		_nonmoving = new Enemy();
		add(_nonmoving);
		_nonmoving.setPosition(80, 60);
		_nonmoving.loadGraphic("assets/images/ghost_placeholder.png", true, 16, 16);
		_nonmoving.animation.add("Idle", [0, 1], 5, true);
		_nonmoving.animation.play("Idle", false, false, -1);

		_nonmoving.setGraphicSize(48);
		_nonmoving.updateHitbox();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
}
