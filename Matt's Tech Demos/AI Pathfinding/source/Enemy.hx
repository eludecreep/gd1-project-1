package;

import flixel.FlxSprite;
import flixel.util.FlxPath;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

class Enemy extends FlxSprite
{
	public function new(x:Int, y:Int):Void
	{
		super();

		makeGraphic(40, 40, FlxColor.RED);
		setPosition(x, y);

		var points:Array<FlxPoint> = [new FlxPoint(80, 80), new FlxPoint(240, 80), new FlxPoint(240, 240), new FlxPoint(80, 240)];
		path = new FlxPath(points);
		path.start(100, FlxPath.LOOP_FORWARD);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
}
