package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;

class PlayState extends FlxState
{
	var _player:Player;
	var _enemy:Enemy;
	override public function create():Void
	{
		super.create();
		
		_player = new Player();
		_player.setPosition(16, 16);
		add(_player);

		_enemy = new Enemy();
		_enemy.setPosition(320, 240);
		add(_enemy);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		if(FlxG.overlap(_player, _enemy.children[0]) && FlxG.pixelPerfectOverlap(_player, _enemy.children[0], 1)) {
			spotted(_player, _enemy);
		}
	}

	private function spotted(_player:Player, _enemy:Enemy):Void {
		add(new FlxText(320, 160, 100, "!", 48));
	}
}
