package;

class YoderLib {
    static public function XOR(cond1:Bool, cond2:Bool) {
        return (cond1 || cond2) && !(cond1 && cond2);
    }
}