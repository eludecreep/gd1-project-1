package;

import flixel.FlxG;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import flixel.addons.display.FlxNestedSprite;

class DetectionCone extends FlxNestedSprite {
    var _angle:Float = Math.PI / 4;
    var _range:Float = 200;
    var _width:Float;
    var _color:FlxColor;

    override public function new() {
        super();
        _width = 2 * _range * FlxMath.fastSin(_angle / 2);
        relativeY = -_width / 2;
        

        _color = new FlxColor(0x40ff0000);
        #if !debug
            visible = false;
        #end

        makeGraphic(Std.int(_range), Std.int(_width), FlxColor.TRANSPARENT);    
        
        FlxSpriteUtil.drawPolygon(this,
                                 [new FlxPoint(0, _width / 2), 
                                  new FlxPoint(_range * FlxMath.fastCos(_angle / 2), 0), 
                                  new FlxPoint(_range * FlxMath.fastCos(_angle / 2), _width)], 
                                  _color);
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
    }
}