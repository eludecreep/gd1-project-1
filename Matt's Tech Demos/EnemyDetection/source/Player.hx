package;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.addons.display.FlxNestedSprite;

class Player extends FlxNestedSprite {
    var speed = 200;
    var _up:Bool = false;
	var _down:Bool = false;
	var _left:Bool = false;
	var _right:Bool = false;
    
    override public function new() {
        super();
        makeGraphic(32, 32, FlxColor.BLUE);
        updateHitbox();
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        poll();
        movement();
    }

    function poll():Void{
		_up = FlxG.keys.anyPressed([UP, W]);
		_down = FlxG.keys.anyPressed([DOWN, S]);
		_left = FlxG.keys.anyPressed([LEFT, A]);
		_right = FlxG.keys.anyPressed([RIGHT, D]);		
	}

    function movement():Void{
		var xSpeed:Float = 0;
        var ySpeed:Float = 0;
        if (_up || _down || _left || _right){
			if(_up) {
                ySpeed -= speed;
            }
            if(_down) {
                ySpeed += speed;
            }
            if(_right) {
                xSpeed += speed;
            }
            if(_left) {
                xSpeed -= speed;
            }

            // Apparently ^ (XOR) doesn't work on Bools despite being a bitwise operation)
            // I wrote my own to make code that needs an XOR look less like a compiler with food poisoning came by
            if(YoderLib.XOR(_up, _down) && YoderLib.XOR(_left, _right)) {
                xSpeed *= (Math.sqrt(2) / 2);
                ySpeed *= (Math.sqrt(2) / 2);
            }
            
            velocity.set(xSpeed, ySpeed);
		}
		else velocity.set(0, 0);

        
	}
}