package;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.text.FlxText;
import flixel.addons.display.FlxNestedSprite;

class Enemy extends FlxNestedSprite {
    var _rotSpeed = 90;
    var _player:Player;

    override public function new() {
        super();
        makeGraphic(16, 16, FlxColor.YELLOW);
        add(new DetectionCone());

    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        angle += _rotSpeed * elapsed;
    }
}