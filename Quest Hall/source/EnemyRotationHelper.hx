package;

import flixel.addons.display.FlxNestedSprite;
import flixel.util.FlxColor;

// Pay no attention to the hacky workaround to rotate the sight cone without rotating the sprite.
class EnemyRotationHelper extends FlxNestedSprite {
    public function new() {
        super();
		makeGraphic(1, 1, FlxColor.TRANSPARENT);
		relativeX = 50;
		relativeY = 50;
		origin.set(50, 50);
        add(new DetectionCone());
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
    }
}
