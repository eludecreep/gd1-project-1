package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.input.keyboard.FlxKey;
import flixel.addons.text.FlxTypeText;

class MenuState extends FlxState
{
	
	var _text:FlxText;
	var _text2:FlxText;
	var _textbox:FlxSprite;
	var _select:Bool = false;
	var _examine:Bool = false;
	var _textNumber:Int = 0;
	var _strings:Array<String> = new Array<String>();
	
	override public function create():Void
	{
		super.create();
		populateArray();
		_text = new FlxText(12, FlxG.height-28, 0, _strings[0]);
		_text.setFormat(null, 12, FlxColor.WHITE, LEFT);
		_text2 = new FlxText(12, FlxG.height-28, 0, _strings[1]);
		_text2.setFormat(null, 12, FlxColor.WHITE, LEFT);
		_textbox = new FlxSprite();
		_textbox.loadGraphic("assets/images/textbox.png");
		_textbox.x = 0;
		_textbox.y = FlxG.height - 40;
		add(_textbox);
		add(_text);
		
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		poll();
		swapText();
	}
	
	function populateArray():Void
	{
		_strings.push("Someone left their Data Structures syllabus here. [...]");
		_strings.push("More like Data Struggles, that class was way too tough for freshman year!");
	}
	
	function poll():Void
	{
		_select = FlxG.keys.justPressed.Z;
	}
	
	function swapText():Void
	{
		if (_select && _textNumber == 0){
			add(_text2);
			remove(_text);
			_textNumber++;
		}
		else if (_select && _textNumber == 1){
			remove(_text2);
			remove(_textbox);
			_textNumber++;
		}
	}
}
