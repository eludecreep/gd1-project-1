package;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.util.FlxColor;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import flixel.addons.display.FlxNestedSprite;
/*
	Collision testing:	
		-Tables, walls, etc
	Can't stand under walls

 */
class Player extends FlxNestedSprite{
	public var canInteract:Bool = false;
	public var canMove:Bool = true;
	var speed : Float = 200;
	var _rot : Float = 0;
	var _up : Bool = false;
	var _down : Bool = false;
	var _left : Bool = false;
	var _right : Bool = false;
	public var _crouch : Bool = false;
	public var underTable : Bool = false;
	var rpiSpriteM = FlxAtlasFrames.fromTexturePackerJson("assets/images/FinalAnimation/player_spritesheet.png", "assets/images/FinalAnimation/player_spritesheet.json");
	//public var rpiSprite = new FlxSprite(0,0);

	public function new(X:Int, Y:Int){
		super(X, Y);
		//add(rpiSprite);
		//rpiSprite.frames = rpiSpriteM;
		frames = rpiSpriteM;
	//Add them to the character
		//Multidirectional Crawl
		animation.addByPrefix("crawl_b", "Player_Crawl_back", 4, true);
		animation.addByPrefix("crawl_f", "Player_Crawl_front", 4, true);
		animation.addByPrefix("crawl_l", "Player_Crawl_left", 5, true);
		animation.addByPrefix("crawl_r", "Player_Crawl_right", 5, true);
		//Multidirectional Idle
		animation.addByPrefix("idle_b", "Player_Idle_back", 4, true);
		animation.addByPrefix("idle_f", "Player_Idle_front", 4, true);
		animation.addByPrefix("idle_l", "Player_Idle_left", 4, true);
		animation.addByPrefix("idle_r", "Player_Idle_right", 4, true);
		//Back and Front, left and right
		animation.addByPrefix("back", "Player_Walk_back", 6, true);
		animation.addByPrefix("front", "Player_Walk_front", 6, true);
		animation.addByPrefix("left", "Player_Walk_left", 6, true);
		animation.addByPrefix("right", "Player_Walk_right", 6, true);
		//Play the Idle animation
		animation.play("idle_f");
		offset.set(20, 2);
		setSize(60, 60);
		//y = 600;
//		rpiSprite.drag.x = rpiSprite.drag.y = 1600;
	}
	override public function update(elapsed : Float):Void{
		super.update(elapsed);
		poll();
		movement();
	}
	function poll():Void{
		_up = FlxG.keys.anyPressed([UP, W]);
		_down = FlxG.keys.anyPressed([DOWN, S]);
		_left = FlxG.keys.anyPressed([LEFT, A]);
		_right = FlxG.keys.anyPressed([RIGHT, D]);
		if (!underTable) {
			_crouch = FlxG.keys.pressed.CONTROL;
		} else {
			_crouch = true;
		}
	}
	/*
	   ADD A HOLD DOWN CTRL BUTTON FOR CRAWLING IN-GAME
	*/
	function movement():Void{
		if(canMove){
			if(_up && _down)
				_up = _down = false;
			if(_left && _right)
				_left = _right = false;
			if(_up || _down || _left || _right){
				if(!_crouch){
					velocity.x = speed;
					velocity.y = speed;
				}else{
					velocity.x = speed/2;
					velocity.y = speed/2;
				}
				if(_up){
					_rot = -90;
					if(_left)
						_rot -= 45;
					else if (_right)
						_rot += 45;
					facing = FlxObject.UP;
				} else if (_down){
					_rot = 90;
					if(_left)
						_rot += 45;
					else if(_right)
						_rot -= 45;
					facing = FlxObject.DOWN;
				}else if (_left){
					_rot = 180;
					facing = FlxObject.LEFT;
				} else if (_right){
					_rot = 0;
					facing = FlxObject.RIGHT;
				}
				if(!_crouch)
					velocity.set(speed, 0);
				else
					velocity.set(speed/2, 0);
				velocity.rotate(FlxPoint.weak(0,0), _rot);
				if(velocity.x != 0 || velocity.y!=0){
					if(!_crouch){
						switch(facing){
							case FlxObject.LEFT:
								animation.play("left");
							case FlxObject.RIGHT:
								animation.play("right");
							case FlxObject.UP:
								animation.play("back");
							case FlxObject.DOWN:
								animation.play("front");
						}
					}else{
						switch(facing){
							case FlxObject.LEFT:
								animation.play("crawl_l");
							case FlxObject.RIGHT:
								animation.play("crawl_r");
							case FlxObject.UP:
								animation.play("crawl_b");
							case FlxObject.DOWN:
								animation.play("crawl_f");
						}
					}
				}
			} else{
				velocity.set(0, 0);
				
				if (_crouch) {
					animation.stop();
				}
				else {
					switch(facing){
						case FlxObject.LEFT:
							animation.play("idle_l");
						case FlxObject.RIGHT:
							animation.play("idle_r");
						case FlxObject.UP:
							animation.play("idle_b");
						case FlxObject.DOWN:
							animation.play("idle_f");
					}
				}
			}
		}
		else{
			velocity.set(0, 0);
			
			if (_crouch) {
				animation.stop();
			}
			else {
				switch(facing){
					case FlxObject.LEFT:
						animation.play("idle_l");
					case FlxObject.RIGHT:
						animation.play("idle_r");
					case FlxObject.UP:
						animation.play("idle_b");
					case FlxObject.DOWN:
						animation.play("idle_f");
				}
			}
		}
	}
}
