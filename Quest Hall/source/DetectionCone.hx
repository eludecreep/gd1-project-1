package;

import flixel.FlxG;
import flixel.tile.FlxTilemap;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import flixel.addons.display.FlxNestedSprite;

class DetectionCone extends FlxNestedSprite {
    var _visionAngle:Float = Math.PI / 4;
    var _range:Float = 400;
    var _width:Float;
    var _color:FlxColor;
    var _walls:FlxTilemap;

    override public function new() {
        super();
        _width = 2 * _range * FlxMath.fastSin(_visionAngle / 2);
        relativeY = -_width / 2;
        

        _color = new FlxColor(0x06dddddd);

        var squareDimension:Int = Std.int(Math.max(_range, _width));
        makeGraphic(squareDimension, squareDimension, FlxColor.TRANSPARENT);


        FlxSpriteUtil.drawPolygon(this,
                                 [new FlxPoint(0, _width / 2), 
                                  new FlxPoint(_range * FlxMath.fastCos(_visionAngle / 2), 0), 
                                  new FlxPoint(_range * FlxMath.fastCos(_visionAngle / 2), _width)], 
                                  _color);
        FlxSpriteUtil.flicker(this, 0, 0, false, false);
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
    }
}