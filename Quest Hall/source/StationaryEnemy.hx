package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;

class StationaryEnemy extends Enemy {
    var _timeBetweenTurns:Float;
    var _timer:Float;
    var _directions:Array<Int>;
    var _currentFacing:Int;
	var _SpriteSheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/FinalAnimation/security_stationary_spritesheet.png", "assets/images/FinalAnimation/security_stationary_spritesheet.json");

    public function new(x:Int, y:Int, directions:Array<Int>, timeBetweenTurns:Float) {
        super(x, y);
        _timer = _timeBetweenTurns = timeBetweenTurns;
        _directions = directions;
        _currentFacing = 0;
        children[0].relativeAngle = _directions[_currentFacing];
		
		frames = _SpriteSheet;
		
		animation.addByPrefix("look_back", "Security_Stationary_back", 5, true);
		animation.addByPrefix("look_front", "Security_Stationary_front", 5, true);
		animation.addByPrefix("look_left", "Security_Stationary_left", 5, true);
		animation.addByPrefix("look_right", "Security_Stationary_right", 5, true);
		
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        _timer -= elapsed;
        if( _timer <= 0 ) {
            _timer = _timeBetweenTurns;
            _currentFacing = ++_currentFacing % _directions.length;
			switch(_directions[_currentFacing]) {
				case 0:
					animation.play("look_right");
				case 90:
					animation.play("look_front");
				case 180:
					animation.play("look_left");
				case 270:
					animation.play("look_back");
				
			}
            children[0].relativeAngle = _directions[_currentFacing];
            // children[0].updateHitbox();
        }
    }
}
