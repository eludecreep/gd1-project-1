package;

import flixel.addons.editors.tiled.TiledLayer;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledTileSet;
import flixel.addons.editors.tiled.TiledTilePropertySet;
import flixel.FlxG;
import flixel.util.FlxPath;
import flixel.FlxObject;
import flixel.group.FlxGroup;
import flixel.tile.FlxTilemap;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import haxe.io.Path;

//Some(Most) of this code was pieced together from other flixel demos, such as
//TiledLevel and GridMovement.

class TiledLevel extends TiledMap
{
	private inline static var c_PATH_LEVEL_TILESHEETS = "assets/tiled/";
	
	// Variables to store all the stuff loaded in from the tiled file
	public var player:Player;
	public var guards:FlxTypedGroup<Enemy>;
	public var checkpoints:FlxTypedGroup<Checkpoint>;
	public var background:FlxGroup;
	public var walls:FlxGroup;
	public var details:FlxGroup;
	public var exit:FlxSprite;
	public var trigger:TextTrigger;
	public var changx:Float;
	public var changy:Float;
	private var collidableTileLayers:Array<FlxTilemap>;
	private var crawlableTileLayers:Array<FlxTilemap>;
	
	public function new(tiledLevel:Dynamic) {
		
		super(tiledLevel);
		
		background = new FlxGroup();
		walls = new FlxGroup();
		details = new FlxGroup();
		
		//Setting max camera spread to the level size in tiled
		FlxG.camera.setScrollBoundsRect(0, 0, fullWidth, fullHeight, true);
		
		
		// Time to load the real level
		for (layer in layers) {
			if (layer.type != TiledLayerType.TILE) continue;
			var tileLayer:TiledTileLayer = cast layer;
			
			var tileLayerName:String = tileLayer.properties.get("tileset");
			
			var tileSet:TiledTileSet = null;
			for (ts in tilesets)
			{
				if (ts.name == tileLayerName)
				{
					tileSet = ts;
					break;
				}
			}
			
			var imagePath = new Path(tileSet.imageSource);
			var processedPath = c_PATH_LEVEL_TILESHEETS + imagePath.file + "." + imagePath.ext;
			var tilemap:FlxTilemap = new FlxTilemap();
			tilemap.loadMapFromArray(tileLayer.tileArray, width, height, processedPath,
				tileSet.tileWidth, tileSet.tileHeight, OFF, tileSet.firstGID, 1, 1);
			//For background
			if (tileLayer.properties.contains("nocollide"))
			{
				if (tileLayer.properties.contains("over")) {
					details.add(tilemap);
				} else {
					background.add(tilemap);
				}
			}
			else //For walls/tables
			{
				if (tileLayer.properties.contains("istable")) {
					if (crawlableTileLayers == null)
						crawlableTileLayers = new Array<FlxTilemap>();
					
					walls.add(tilemap);
					crawlableTileLayers.push(tilemap);
					
				} else {
					if (collidableTileLayers == null)
						collidableTileLayers = new Array<FlxTilemap>();
					
					walls.add(tilemap);
					collidableTileLayers.push(tilemap);
				}
			}	
		}
	}
	
	public function loadObjects() {
		// load all non-background and wall things.
		for (layer in layers) {
			if (layer.type != TiledLayerType.OBJECT)
				continue;
			var objectLayer:TiledObjectLayer = cast layer;

			for (o in objectLayer.objects) {
				loadObject( o, objectLayer);
			}
		}
	}
	
	public function loadObject(o:TiledObject, g:TiledObjectLayer) {
		var x:Int = o.x;
		var y:Int = o.y;
		
		if (o.gid != -1)
			y -= g.map.getGidOwner(o.gid).tileHeight;
		
		switch (o.type.toLowerCase()) {
			case "player_start": // Based on the location in tiled, makes a new player
				player = new Player(x, y);
				
			case "guard_start":
				var turntime = Std.parseFloat(o.properties.get("turntime"));
				var turntype = o.properties.get("turntype");
				var guy:Enemy; 
				if (turntype == "lr") {
					guy = new StationaryEnemy(x, y, [0, 180], turntime);
				} else if (turntype == "ud") {
					guy = new StationaryEnemy(x, y, [90, 270], turntime);
				} else {
					guy = new StationaryEnemy(x, y, [0, 90, 180, 270], turntime);
				}
				if (guards == null)
					guards = new FlxTypedGroup<Enemy>();
				guards.add(guy);
				
			case "mguard_start":
				var speed = Std.parseFloat(o.properties.get("speed"));
				var mode = o.properties.get("mode");
				var points = o.points;
				for (point in points) {
					point.x = x + point.x - 50;
					point.y = y + point.y - 50;
				}
				var nurse:Bool = false;
				if (o.properties.get("nurse") == "true")
					nurse = true;
				
				var guy:Enemy;
				if (mode == "yoyo") {
					guy = new MobileEnemy(x, y, points, FlxPath.YOYO, speed, nurse);
				} else {
					guy = new MobileEnemy(x, y, points, FlxPath.LOOP_FORWARD, speed, nurse);
				}
								
				if (guards == null)
					guards = new FlxTypedGroup<Enemy>();
				guards.add(guy);
				
			case "checkpoint":
				var d = o.properties.get("description");
				var check : Checkpoint;
				check = new Checkpoint(x, y, d);
				if(checkpoints == null)
					checkpoints = new FlxTypedGroup<Checkpoint>();
				checkpoints.add(check);

			case "load_level":
				exit = new FlxSprite(x, y);
				exit.makeGraphic(100, 100, FlxColor.TRANSPARENT);
				
			case "texttrigger":
				trigger = new TextTrigger(x, y, o.properties.get("description"));
				if (o.properties.get("ischang") == "true") {
					trigger._isChang = true;
				}
			case "chang":
				changx = x;
				changy = y;
		}
		
	}
	
	public function collideWithLevel(obj:FlxObject, ?notifyCallback:FlxObject->FlxObject->Void, ?processCallback:FlxObject->FlxObject->Bool):Bool
	{
		player.underTable = false;
		if (collidableTileLayers == null)
			return false;
		
		if (crawlableTileLayers != null) {
			for (map in crawlableTileLayers)
			{
				if (!player._crouch) {
					if (FlxG.overlap(map, obj, notifyCallback, processCallback != null ? processCallback : FlxObject.separate))
					{
						
						return true;
					}
				} else {
					if (map.overlaps(obj))
					{
						player.underTable = true;
					}
					
				}
			}
		}

		for (map in collidableTileLayers)
		{
			// IMPORTANT: Always collide the map with objects, not the other way around.
			//            This prevents odd collision errors (collision separation code off by 1 px).
			if (FlxG.overlap(map, obj, notifyCallback, processCallback != null ? processCallback : FlxObject.separate))
			{
				return true;
			}
		}
		
		
		
		
		return false;
	}

	public function getCollidableLayers() {
		return collidableTileLayers;
	}
	
}
