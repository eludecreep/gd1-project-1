package;

import flixel.FlxState;
import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.util.FlxPath;
import flixel.system.scaleModes.FillScaleMode;

class EnemyTestState extends FlxState
{
	
	public var _player:Player;
    public var _enemy:Enemy;
	private var _level:TiledLevel;
	
	override public function create():Void
	{
		FlxG.camera.setSize(900, 900);
		FlxG.camera.setScale(1, 1);
		new FillScaleMode();
		
		if (FlxG.sound.music == null)   // don't restart the music if it's already playing
		{
			FlxG.sound.playMusic(AssetPaths.MainMusic__ogg, 1, true);
		}
        _player = new Player(480, 360);
        add(_player);

		// _enemy = new StationaryEnemy(320, 240, [0, 90, 180, 270], 2.0);
		var points:Array<FlxPoint> = [new FlxPoint(320, 240), new FlxPoint(320, 480), new FlxPoint(640, 480), new FlxPoint(640, 240)];
		_enemy = new MobileEnemy(0, 0, points, FlxPath.YOYO, 100);
		add(_enemy);
		FlxG.camera.follow(_player, TOPDOWN_TIGHT, 1);
		FlxG.camera.targetOffset.set(0, -50);
		
		// add(new FlxText(320, 160, 100, Std.string(new FlxPoint(0, 0).angleBetween(new FlxPoint(-1, -1))), 48));

		super.create();
		FlxG.watch.add(_enemy, "_path");
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
        if(FlxG.overlap(_player, _enemy.getVisionCone()) && FlxG.pixelPerfectOverlap(_player, _enemy.getVisionCone(), 1)) {
			spotted(_player, _enemy);
		}
	}

    private function spotted(_player:Player, _enemy:Enemy):Void {
		add(new FlxText(320, 160, 100, "!", 48));
	}


}
