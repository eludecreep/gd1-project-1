package;

import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.system.scaleModes.FillScaleMode;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class PlayState extends FlxState
{
	var interactX:Int;
	var interactY:Int;
	public var spawnX:Float;
	public var spawnY:Float;
	public var player:Player;
	var _gameover:FlxSprite;
	var _isGameOver:Bool = false;
	var _isNextLevel:Bool = false;
	var _counter:Float;
	var _textbox:FlxSprite;
	var _text:FlxText;
	private var _level:TiledLevel;
	private var _soundGameOver:FlxSound;
	private var _soundInteract:FlxSound;
	var _stringText:String = "";
	var temp : Checkpoint;
	
	override public function create():Void
	{	
		FlxG.mouse.visible = false;	
		FlxG.camera.setSize(900, 900);
		FlxG.camera.setScale(1, 1);
		new FillScaleMode();
		FlxG.camera.fade(FlxColor.BLACK, 1, true);
		
		if (FlxG.sound.music == null) // don't restart the music if it's already playing
		{
			FlxG.sound.playMusic(AssetPaths.MainMusic__ogg, 1, true);
		}
		
		_level = new TiledLevel("assets/tiled/Level1.tmx");
		//_level = new TiledLevel("assets/tiled/Level2.tmx");
		//_level = new TiledLevel("assets/tiled/Level3.tmx");
		add(_level.background);
		_level.loadObjects();
		player = _level.player;
		spawnX = player.x;
		spawnY = player.y;
		add(player);
		add(_level.guards);
		add(_level.walls);
		add(_level.details);
		add(_level.exit);
		add(_level.checkpoints);
		
		FlxG.camera.follow(player, TOPDOWN_TIGHT, 1);
		FlxG.camera.targetOffset.set(0, -50);
		
		_gameover = new FlxSprite();
		_gameover.loadGraphic("assets/images/gameover.png");
		_soundGameOver = FlxG.sound.load(AssetPaths.rip__wav);
		_soundInteract = FlxG.sound.load(AssetPaths.Interact__wav);
		_textbox = new FlxSprite();
		_textbox.loadGraphic("assets/images/textbox2.png");
		
		super.create();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		for (c in _level.checkpoints){
			examineObject(c);
		}
		if (_isGameOver){
			if (FlxG.keys.justPressed.E){
				_isGameOver = false;
				remove(_gameover);
				player.x = spawnX + 50;
				player.y = spawnY + 60;
				FlxG.sound.music.resume();
				player.canMove = true;
			}
		}
		
		if (_isNextLevel) {
			_counter -= elapsed;
			if (_counter <= 0) {
				FlxG.switchState(new LevelTwo());
			}
			
		}
		
		_level.collideWithLevel(player);
		
		
		gameOver();
		nextLevel();
	}
	
	function examineObject(C : Checkpoint):Void{
		if (FlxG.keys.justPressed.E && FlxG.overlap(player, C) && !C._isExamine){
			_soundInteract.play();
			C._isExamine = true;
			player.canMove = false;
			_textbox.x = FlxG.camera.scroll.x;
			_textbox.y = FlxG.camera.scroll.y + 820;
			_text = new FlxText(FlxG.camera.scroll.x+12, FlxG.camera.scroll.y+832, 876, C.getDescription());
			_text.setFormat(null, 12, FlxColor.WHITE, LEFT);
			add(_textbox);
			add(_text);
			spawnX = C.getX();
			spawnY = C.getY();
			temp = C;
		}
		else if (FlxG.keys.justPressed.E && C._isExamine){
			C._isExamine = false;
			player.canMove = true;
			remove(_textbox);
			remove(_text);
		}
	}
	
	public function gameOver():Void{
		for (i in _level.guards){
			if (FlxG.overlap(player, i.getVisionCone()) && FlxG.pixelPerfectOverlap(player, i.getVisionCone(), 1) && !_isGameOver && !player.underTable
			&& _level.getCollidableLayers()[0].ray(i.getPosition(), player.getPosition())){
				
				
				//Sound effect for death
				FlxG.sound.music.pause();
				_soundGameOver.play();
				player.canMove = false;
				add(_gameover);
				_gameover.x = FlxG.camera.scroll.x + 200;
				_gameover.y = FlxG.camera.scroll.y + 300;
				_isGameOver = true;
				
			}
		}
	}
	
	public function nextLevel():Void{
		if (FlxG.overlap(player, _level.exit) && !_isNextLevel){
			_isNextLevel = true;
			_counter = 1;
			FlxG.camera.fade(FlxColor.BLACK, 1, false);
		}
	}
}
