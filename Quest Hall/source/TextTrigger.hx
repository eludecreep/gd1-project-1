package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.util.FlxColor;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import flixel.addons.display.FlxNestedSprite;
import flixel.system.FlxSound;

class TextTrigger extends FlxNestedSprite{
	private var description:String;
	private var X : Int;
	private var Y : Int;
	public var _isExamine : Bool = false;
	public var _isDone:Bool = false;
	public var _isChang:Bool = false;
	public var _changTalk:FlxSound = FlxG.sound.load(AssetPaths.Im_Professor_Chang_and_im_terrified__wav);

	public function new(x:Int, y:Int, d:String){
		super(x, y);
		X = x;
		Y = y;
		description = d;
		makeGraphic(100, 100, FlxColor.TRANSPARENT);
	}
	override public function update(elapsed:Float){
		super.update(elapsed);
	}
	public function getDescription():String{
		return description;
	}
	public function getX():Int{
		return X;
	}
	public function getY():Int{
		return Y;
	}	
}

