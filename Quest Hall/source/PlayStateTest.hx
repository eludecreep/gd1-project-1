package;

import flixel.FlxSprite;
import flixel.FlxState;
import flixel.util.FlxColor;
import flixel.FlxG;
import flixel.text.FlxText;
import Std;

class PlayStateTest extends FlxState
{
	var startX:Int = 320;
	var startY:Int = 200;
	var interactX:Int = 999;
	var interactY:Int = 999;
	var _textNumber:Int = 0;
	var _player:Player;
	var _examine:FlxSprite;
	var _kill:FlxSprite;
	var _text:FlxText;
	var _text2:FlxText;
	var _textbox:FlxSprite;
	var _isExamine:Bool = false;
	var _strings:Array<String> = new Array<String>();
	
	override public function create():Void
	{
		super.create();
		populateArray();
		_player = new Player(startX,startY);
		_examine = new FlxSprite();
		_examine.makeGraphic(30, 30, FlxColor.YELLOW);
		_kill = new FlxSprite();
		_kill.makeGraphic(30, 30, FlxColor.RED);
		_kill.x = 400;
		_kill.y = 10;
		_text = new FlxText(12, FlxG.height-28, 0, _strings[0]);
		_text.setFormat(null, 12, FlxColor.WHITE, LEFT);
		_text2 = new FlxText(12, FlxG.height-28, 0, _strings[1]);
		_text2.setFormat(null, 12, FlxColor.WHITE, LEFT);
		_textbox = new FlxSprite();
		_textbox.loadGraphic("assets/images/textbox.png");
		_textbox.x = 0;
		_textbox.y = FlxG.height - 40;
		add(_examine);
		add(_kill);
		add(_player);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (FlxG.overlap(_player, _kill)){
			_player.x = startX;
			_player.y = startY;
		}
		if (FlxG.overlap(_player, _examine)){
			_player.canInteract = true;
			interactX = Std.int(_examine.x);
			interactY = Std.int(_examine.y);
		}
		else{
			_player.canInteract = false;
		}
		
		examineObject();
		
	}
	
	function populateArray():Void
	{
		_strings.push("Someone left their Data Structures syllabus here. [...]");
		_strings.push("More like Data Struggles, that class was way too tough for freshman year!");
	}
	
	function examineObject():Void{
		if (FlxG.keys.justPressed.E && _player.canInteract && !_isExamine){
			_isExamine = true;
			_player.canMove = false;
			add(_textbox);
			add(_text);
			startX = interactX;
			startY = interactY;
		}
		else if (FlxG.keys.justPressed.E && _isExamine && _textNumber == 0){
			//_isExamine = false;
			//_player.canMove = true;
			//remove(_textbox);
			remove(_text);
			add(_text2);
			_textNumber++;
		}
		else if (FlxG.keys.justPressed.E && _isExamine && _textNumber == 1){
			_isExamine = false;
			_player.canMove = true;
			remove(_textbox);
			//remove(_text);
			remove(_text2);
			_textNumber--;
		}
	}
}
