package;

import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.system.scaleModes.FillScaleMode;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class OpeningCutscene extends FlxState
{
	
	private var thing:FlxSprite;
	var order = ["frame1", "frame2", "frame3", "frame4", "frame5", "frame6"];
	var current:Int = 0;
	var _sndthunder:FlxSound;
	var _sndphone:FlxSound;
	var timer:Float;
	var done = false;
	
	override public function create():Void
	{
		FlxG.mouse.visible = false;
		FlxG.camera.setSize(900, 900);
		FlxG.camera.setPosition(0, 0);
		FlxG.camera.setScale(1, 1);
		new FillScaleMode();
		FlxG.camera.fade(FlxColor.BLACK, 1, true);
		
		FlxG.sound.playMusic(AssetPaths.OpeningMusic__ogg, 2, true);
		
		thing = new FlxSprite();
		thing.loadGraphic("assets/images/FinalAnimation/openingcinematic_spritesheet.png", true, 900, 900);
		thing.animation.add("frame1", [0], 1, false);
		thing.animation.add("frame2", [1], 1, false);
		thing.animation.add("frame3", [2], 1, false);
		thing.animation.add("frame4", [3], 1, false);
		thing.animation.add("frame5", [4], 1, false);
		thing.animation.add("frame6", [5], 1, false);
		
		add(thing);
		add(new FlxText(10, 10, 300, "HI", 16));
		thing.animation.play("frame1");
		
		_sndthunder = FlxG.sound.load("assets/sounds/Thunder.wav");
		_sndphone = FlxG.sound.load("assets/sounds/Phone.wav");
		
		super.create();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		if (FlxG.keys.justPressed.ANY) {
			current += 1;
			if (current < 6) {
				thing.animation.play(order[current]);
				if (current == 1)
					_sndphone.play();
				if (current == 5)
					_sndthunder.play();
			} else if (current == 6) {
				FlxG.sound.playMusic(AssetPaths.MainMusic__ogg, 1, true);
				
				FlxG.camera.fade(FlxColor.BLACK, 1, false);
				done = true;
				timer = 1;
			}
		}
		if (done) {
			timer -= elapsed;
			if (timer <= 0) {
				FlxG.switchState(new PlayState());
			}
		}
	}
}
