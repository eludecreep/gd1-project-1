package;

import flixel.addons.display.FlxNestedSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

class Enemy extends FlxNestedSprite {
	

    public function new(x:Int, y:Int) {
        super(x, y);
        add(new EnemyRotationHelper());
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
    }

    public function getVisionCone() {
        return children[0].children[0];
    }

    public function getFacing() {
        return children[0].relativeAngle;
    }
}