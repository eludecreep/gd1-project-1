package;

import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxPath;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.graphics.frames.FlxFrame;

class MobileEnemy extends Enemy {

    var _lastPos:FlxPoint;
    var _path:Array<FlxPoint>;
    var _nodeCounter:Int;
    var _speed:Float;
    var _deg2Rad:Float;
    var _timer:Float;
	var _SpriteSheet = FlxAtlasFrames.fromTexturePackerJson("assets/images/FinalAnimation/ghost_normal_spritesheet.png", "assets/images/FinalAnimation/ghost_normal_spritesheet.json");
	var _SpriteSheetSpecial = FlxAtlasFrames.fromTexturePackerJson("assets/images/FinalAnimation/ghost_nurse_spritesheet.png", "assets/images/FinalAnimation/ghost_nurse_spritesheet.json");

    public function new(x:Int, y:Int, myPath:Array<FlxPoint>, mode:Int, speed:Float, special:Bool) {
        super(x, y);
        
        _deg2Rad = Math.PI / 180;

        _path = myPath;
        
        _nodeCounter = 0;
        
        if(mode == FlxPath.YOYO) {
            var i:Int = myPath.length - 2;
            while(i > 0)
            {
                _path.push(myPath[i]);
                --i;
            }
        }

        _lastPos = getPosition();

		_speed = speed;

        
		//makeGraphic(100, 100, FlxColor.BLACK);
        _timer = Math.sqrt(Math.pow(_path[_nodeCounter].x - _lastPos.x, 2) + Math.pow(_path[_nodeCounter].y - _lastPos.y, 2)) / _speed;
		
		//Animation stuff
		if (special) {
			frames = _SpriteSheetSpecial;
			
			animation.addByPrefix("back", "Nurse_Ghost_Walk_back", 5, true);
			animation.addByPrefix("front", "Nurse_Ghost_Walk_front", 5, true);
			animation.addByPrefix("left", "Nurse_Ghost_Walk_left", 5, true);
			animation.addByPrefix("right", "Nurse_Ghost_Walk_right", 5, true);
		} else {
			frames = _SpriteSheet;
			
			animation.addByPrefix("back", "Ghost_Normal_Walk_back", 5, true);
			animation.addByPrefix("front", "Ghost_Normal_Walk_front", 5, true);
			animation.addByPrefix("left", "Ghost_Normal_Walk_left", 5, true);
			animation.addByPrefix("right", "Ghost_Normal_Walk_right", 5, true);
		}
		
    }

    override public function update(elapsed:Float) {
        super.update(elapsed);
        if(_timer > 0) {
            _timer -= elapsed;
        }
        else {
            getNextNode();
        }
        
        children[0].relativeAngle = (_lastPos.angleBetween(_path[_nodeCounter]) - 90 + 720) % 360;
		var angle = getAngle();
		angle = (angle % 360);
		if(angle <= 45 ) {
			// Face Rightward
			animation.play("right");
		}
		else if(angle > 45 && angle < 135) {
			// Face Downward
			animation.play("front");
		}
		else if(angle >= 135 && angle <= 225) {
			// Face Leftward
			animation.play("left");
		}
		else if(angle > 225 && angle < 315) {
			// Face Upward
			animation.play("back");
		}
		else if(angle >= 315 ) {
			// Also Face Rightward
			animation.play("right");
		}
        x += _speed * elapsed * FlxMath.fastCos(children[0].relativeAngle * _deg2Rad);
        y += _speed * elapsed * FlxMath.fastSin(children[0].relativeAngle * _deg2Rad);
    }

    public function getAngle() {
        return children[0].relativeAngle;
    }

    
    
     

     public function getNextNode() {
         _lastPos = _path[_nodeCounter];
         ++_nodeCounter;
         _nodeCounter %= _path.length;
         _timer = Math.sqrt(Math.pow(_path[_nodeCounter].x - _lastPos.x, 2) + Math.pow(_path[_nodeCounter].y - _lastPos.y, 2)) / _speed;
     }
}